#!/bin/bash
set -e

./gradlew clean build

docker build . -t micronaut-app
docker run --rm --entrypoint cat micronaut-app  /home/application/function.zip > build/function.zip
unzip build/function.zip -d build/function

