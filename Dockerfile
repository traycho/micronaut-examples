FROM alpine:3.7 as builder
WORKDIR /home/application
COPY build /home/application/build
COPY bootstrap /home/application/bootstrap
RUN apk add zip

FROM oracle/graalvm-ce:19.2.0 as graalvm
COPY --from=builder /home/application/ /home/application/

WORKDIR /home/application
RUN gu install native-image
RUN native-image --no-server -jar build/libs/*-all.jar

FROM builder
COPY --from=graalvm /home/application/server /home/application/server
COPY --from=graalvm /opt/graalvm-ce-19.2.0/jre/lib/security/cacerts /home/application/cacerts
COPY --from=graalvm /opt/graalvm-ce-19.2.0/jre/lib/amd64/libsunec.so /home/application/libsunec.so
RUN chmod 755 bootstrap
RUN chmod 755 server
RUN zip -j function.zip bootstrap server cacerts libsunec.so
EXPOSE 8080
ENTRYPOINT ["/home/application/server"]
