Application works fine if not build for graavm execution.

```
# Start application server`
./gradlew run
```

```
# Execute ping
curl -G http://localhost:8080/execute?filter=1&filter=2&filter=3

# Actual response
{
    ["1","2","3"]
}
```

#  Step to reproduce an issue in sam local:
```
# Build native image
./build-native-image.sh

# Run sam local (expecting aws-sam-cli to be installed)
sam local start-api 

# Execute ping results in printing only the first filter param
curl -G http://localhost:3000/execute?filter=1&filter=2&filter=3

["1"] is printed instead of ["1","2","3"]

```
