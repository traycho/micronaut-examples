package com.micronaut.app.exception;

public class JwtValidationException extends RuntimeException {

    /**
     * Status code.
     */
    private int status;

    /**
     * Default constructor.
     */
    public JwtValidationException() {
    }

    /**
     * Constructor.
     *
     * @param message error message
     * @param status  status code
     */
    public JwtValidationException(final String message, final int status) {
        super(message);
        this.status = status;
    }

    /**
     * Gets status code.
     *
     * @return status code.
     */
    public int getStatusCode() {
        return status;
    }
}
