package com.micronaut.app.exception.handler;

import com.micronaut.app.dto.MessageResponse;
import com.micronaut.app.exception.JwtValidationException;
import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;

import javax.inject.Singleton;

@Produces
@Singleton
@Requires(classes = {JwtValidationException.class, ExceptionHandler.class})
public class JwtValidationExceptionHandler implements ExceptionHandler<JwtValidationException, HttpResponse> {

    @Override
    public HttpResponse handle(final HttpRequest request, final JwtValidationException exception) {
        final HttpStatus status = HttpStatus.valueOf(exception.getStatusCode());
        final MutableHttpResponse<Object> response = HttpResponse.status(status);
        final String reason = exception.getMessage();
        MessageResponse message = MessageResponse.builder().message(reason).build();

        return response.contentType(MediaType.APPLICATION_JSON).body(message);
    }
}
