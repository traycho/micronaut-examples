package com.micronaut.app.interceptor;

import com.micronaut.app.annotation.JwtAuthorized;
import com.micronaut.app.exception.JwtValidationException;
import io.micronaut.aop.MethodInterceptor;
import io.micronaut.aop.MethodInvocationContext;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.context.ServerRequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.util.List;
import java.util.Map;
import java.util.Optional;
@Singleton
public class JwtInterceptor implements MethodInterceptor<Object, Object> {

    /**
     * Default logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(JwtInterceptor.class);



    /**
     * Intercept a method and validating jwt annotations.
     *
     * @param context method invocation context
     * @return an object processed by the method
     */
    @Override
    public Object intercept(final MethodInvocationContext<Object, Object> context) {
        final Optional<HttpRequest<Object>> currentRequest = ServerRequestContext.currentRequest();
        final HttpRequest<Object> httpRequest = currentRequest.get();
        final Map<String, List<String>> headers = httpRequest.getHeaders().asMap();
        LOG.info("Headers: {}", headers);

        String[] apps = context.getAnnotationMetadata().getValue(JwtAuthorized.class, "apps", String[].class).orElse(null);
        if (apps != null && apps.length > 0) {
            List<String> appIds = headers.get("appId");
            if (appIds == null || appIds.size() != 1) {
                throw new JwtValidationException("Missing appId header", 401);
            }
        }
        return context.proceed();
    }
}
