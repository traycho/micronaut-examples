package com.micronaut.app.annotation;

import com.micronaut.app.interceptor.JwtInterceptor;
import io.micronaut.aop.Around;
import io.micronaut.context.annotation.Type;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specify which apps are authorized to call this resource.
 * <pre>
 *     <code>@JwtAuthorizedFor(apps = {"app1", "app2"})</code>
 * </pre>
 */
@Around
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Type(JwtInterceptor.class)
public @interface JwtAuthorized {

    /**
     * A list of authorized applications.
     * @return an array
     */
    String[] apps();
}
