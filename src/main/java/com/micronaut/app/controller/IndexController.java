package com.micronaut.app.controller;

import com.micronaut.app.annotation.JwtAuthorized;
import com.micronaut.app.dto.MessageResponse;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpResponseFactory;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import java.util.List;
import javax.ws.rs.QueryParam;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class IndexController {

    @Get(uri = "/ping")
    HttpResponse ping() {
        MessageResponse body = MessageResponse.builder().message("ping").build();
        return buildResponse(body, HttpStatus.ACCEPTED);
    }

    @Get("/execute{?filter*}")
    HttpResponse filter(@QueryParam("filter") List<String> filter) {
        log.info("Filter={}", filter);
        return buildResponse(filter, HttpStatus.OK);
    }

    @Get(uri = "/pong")
    MessageResponse index() {
        return MessageResponse.builder().message("pong").build();
    }

    @Get(uri = "/secured")
    @JwtAuthorized(apps = {"app"})
    MessageResponse secured() {
        return MessageResponse.builder().message("secured").build();
    }


    HttpResponse buildResponse(final Object body, final HttpStatus status) {
        return HttpResponseFactory.INSTANCE.status(status)
                .body(body);
    }
}
